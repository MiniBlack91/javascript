String.prototype.ucfist=function () {
    if (arguments.length !== 1) {
        throw new Error("Expected only one argument");
    }
    if (typeof this !== "string") {
        return "";
    }
    const firstLetter = this.slice(0, 1);
    const firstLetterUppercase = firstLetter.toUpperCase();
    const rest = this.slice(1);
    return firstLetterUppercase + rest;
}

String.prototype.vig=function(key) {
    if (arguments.length !== 2) {
        throw new Error("Expected exactly two arguments");
    }
    if (typeof this !== "string") {
        return "";
    }
    if (typeof key !== "string") {
        throw new TypeError("Second argument expected to be a string");
    }
    if (this.length === 0) {
        throw new ReferenceError("First argument must not be empty");
    }
    if (key.length === 0) {
        throw new ReferenceError("Second argument must not be empty");
    }
    const text = this.toLowerCase();
    const crypter = key.toLowerCase();
    const TEXT_LENGTH = text.length;
    const CRYPTER_LENGTH = crypter.length;
    let result = "";
    for (let index = 0, characterIndex = 0; index < TEXT_LENGTH; index++) {
        if (isLetter(text[index])) {
            result += String.fromCharCode((text.charCodeAt(index) - 97 + crypter.charCodeAt(characterIndex % CRYPTER_LENGTH) - 97) % 26 + 97);
            characterIndex++;
            continue;
        }
        result += text[index];
    }
    return result;
}

Object.prototype.prop_access=function(properties, tested = []) {
    if (properties === null || properties.length === 0) {
        return this;
    }
    if (this === null) {
        return console.log(`${properties} not exist`);
    }
    const [first, ...rest] = properties.split(".");
    tested.push(first);
    if (!(first in this)) {
        const stringified = tested.join(".");
        return console.log(`${stringified} not exist`);
    }
    return prop_access(this[first], rest.join("."), tested);
}